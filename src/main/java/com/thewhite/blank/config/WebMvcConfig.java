// Todo поправить в соответствии с названием проекта
package com.thewhite.blank.config;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.resource.PathResourceResolver;
import org.springframework.web.servlet.resource.TransformedResource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Created by Vdovin S. on 07.09.18.
 *
 * @author Sergey Vdovin
 * @version 1.0
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    private static final String API_PATH = "/app_config";
    private static final String PATH_PATTERNS = "/**";
    private static final String FRONT_CONTROLLER = "index.html";
    private static final String BASE_HREF_PLACEHOLDER = "<!-- #base-href# -->";
    private static final String FRONT_CONTROLLER_ENCODING = StandardCharsets.UTF_8.name();

    private final String contextPath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(PATH_PATTERNS)
                .addResourceLocations("classpath:/public/")
                .resourceChain(true)
                .addResolver(new SinglePageAppResourceResolver());
    }

    public WebMvcConfig(@Value("${ws.options.basePath}") String contextPath) {
        this.contextPath = contextPath;
    }

    private class SinglePageAppResourceResolver extends PathResourceResolver {

        private static final String URL_SEPARATOR = "/";

        private TransformedResource transformedResource(Resource resource) throws IOException {
            String fileContent = IOUtils.toString(resource.getInputStream(), FRONT_CONTROLLER_ENCODING);
            fileContent = fileContent.replace(BASE_HREF_PLACEHOLDER, buildBaseHref());
            return new TransformedResource(resource, fileContent.getBytes());
        }

        private String buildBaseHref() {
            return String.format("<base href=\"%s%s\">", contextPath, URL_SEPARATOR);
        }

        @Override
        protected Resource getResource(String resourcePath, Resource location) throws IOException {
            Resource resource = location.createRelative(resourcePath);
            if (resource.exists() && resource.isReadable()) {
                //Распихиваем <base href> по плейсхолдерам в index.html
                if (resourcePath.contains(FRONT_CONTROLLER)) {
                    return transformedResource(resource);
                }

                return resource;
            }

            //Игнорируем запросы на апи(app_config/config)
            if ((URL_SEPARATOR + resourcePath).startsWith(API_PATH)) {
                return null;
            }

            return null;
        }
    }
}
