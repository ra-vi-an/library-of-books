// import DevTools from 'mobx-react-devtools';
import * as React from 'react';
import IndexHtmlConfig from './modules/Global/components/indexHtmlConfig';
import MainRouter from './routers/mainRouter';

export const AppConfig = () => {
    return (
        <>
            <IndexHtmlConfig/>
            <MainRouter/>
        </>
    );
};
