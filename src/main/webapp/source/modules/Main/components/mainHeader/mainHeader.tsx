import { toast } from 'react-toastify';
import { FlexBox } from '@thewhite/react-flex-layout';
import { GlobalStore } from '@thewhite/react-global-module/lib/store';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import { WsReactBaseComponent, WsReactBaseService } from '@thewhite/react-base-components';
import { WsReactDatepicker } from 'ws-react-datepicker';
import ReactSVG from 'react-svg';
import { IArrServices } from 'source/modules/Global/interfaces/IArrServices';
import { Room } from 'source/modules/Rooms/models/rooms';
import classnames from 'classnames';
import Button from 'source/modules/Global/components/button/button';
import { AuthService } from '../../../Auth/services/AuthService';

interface IMainHeaderProps {
    globalStore?: GlobalStore;
}

interface IMainHeaderState {
    arrRooms: Room[];
    arrImg: string[];
    endDate: string | null;
    startDate: string | null;
    arrServices: IArrServices[];
}

interface IMainHeader {
    props: IMainHeaderProps;
    state: IMainHeaderState;
}

@inject('globalStore')
@observer
export default class MainHeader extends WsReactBaseComponent<IMainHeaderProps, IMainHeaderState> implements IMainHeader {
    state: IMainHeaderState = {
        arrServices: [
            {
                name: 'БЕСПЛАТНЫЙ ЗАВТРАК',
                src: 'assets/images/svg/check-mark.svg',
            },
            {
                name: 'БЕСПЛАТНЫЙ WI-FI',
                src: 'assets/images/svg/check-mark.svg',
            },
            {
                name: 'ПРИЯТНАЯ АТМОСФЕРА',
                src: 'assets/images/svg/check-mark.svg',
            },
        ],
        arrImg: [
            'assets/images/Hotel/room-1.jpg',
            'assets/images/Hotel/room-2.jpg',
            'assets/images/Hotel/room-3.jpg',
            'assets/images/Hotel/room-4.jpg',
            'assets/images/Hotel/room-5.jpg',
            'assets/images/Hotel/room-6.jpg',
        ],
        arrRooms: [],
        endDate: null,
        startDate: null,
    }

    /**
     * Отдаю даты в url
     */
    setDate = () => {
        if (this.state.startDate && this.state.endDate && !!AuthService.getToken()) {
            this.goToState(`/rooms/&startDate=${this.moment(this.state.startDate)
                .format('YYYY-MM-DD')}&endDate=${this.moment(this.state.endDate)
                .format('YYYY-MM-DD')}`)
            window.localStorage.setItem('date', JSON.stringify({ startDate: this.state.startDate, endDate: this.state.endDate }));
        } else {
            WsReactBaseService.showToast(toast.error('Вы не авторизованы'));
        }
    };

    render(): false | JSX.Element {
        return (
            <FlexBox
                row={'ctr'}
                column={'ctr'}
                className={'main-header'}
            >
                <img
                    className={'main-header__main-img'}
                    src={'assets/images/Hotel/main-page.jpg'}
                />
                <FlexBox
                    column={'ctr'}
                    className={'main-header__reservation'}
                >
                    <div
                        className={classnames(
                            'text-subheader-3',
                            'main-header__booking',

                        )}
                    >
                        Забронировать комнату
                    </div>
                    <FlexBox
                        row={'sb ctr'}
                        className={'main-header__reservation-date'}
                    >
                        <div
                            className={classnames(
                                'text-label-1',
                                'main-header__date-arrival',
                            )}
                        >
                            Дата приезда
                        </div>
                        <WsReactDatepicker
                            updateCallback={
                                value => this.updateState(value, 'startDate')
                            }
                        />
                    </FlexBox>
                    <FlexBox
                        row={'sb ctr'}
                        className={'main-header__reservation-date'}
                    >
                        <div
                            className={classnames(
                                'text-label-1',
                                'main-header__date-arrival',
                            )}
                        >
                            Дата отъезда
                        </div>
                        <WsReactDatepicker
                            updateCallback={
                                value => this.updateState(value, 'endDate')
                            }
                        />
                    </FlexBox>
                    <div
                        onClick={this.setDate}
                        className={'main-header__button'}
                    >
                        <Button
                            label={'Показать свободные комнаты'}
                        />
                    </div>
                </FlexBox>
                <FlexBox
                    row={'ctr'}
                    className={'main-header__service'}
                >
                    {
                        this.state.arrServices.map((arrServices: IArrServices, index: number) =>
                            <FlexBox
                                key={index}
                                row={'ctr'}
                                className={'main-header__service-content'}
                            >
                                <ReactSVG
                                    className={'main-header__check-mark'}
                                    src={arrServices.src}
                                />
                                <div>
                                    { arrServices.name }
                                </div>
                            </FlexBox>,
                        )
                    }
                </FlexBox>
                <FlexBox
                    column={'ctr'}
                    className={'main-header__gallery'}
                >
                    <h2
                        className={'main-header__gallery-title'}
                    >
                        Галерея
                    </h2>
                    <FlexBox
                        rowWrap={'ctr'}
                    >
                        {
                            this.state.arrImg.map((img: string, index: number) =>
                                <img
                                    src={img}
                                    key={index}
                                    className={'main-header__gallery-img'}
                                />,
                            )
                        }
                    </FlexBox>
                </FlexBox>
            </FlexBox>
        );
    }
}
