import classnames from 'classnames';
import { inject, observer } from 'mobx-react';
import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { GlobalStore } from '@thewhite/react-global-module/lib/store';
import * as React from 'react';
import MainHeader from '../../components/mainHeader/mainHeader';

interface IMainPageProps {
    globalStore?: GlobalStore;
}

export interface IMainPage extends WsReactBaseComponentInterface {
    props: IMainPageProps;
}

@inject('globalStore')
@observer
export default class MainPage extends WsReactBaseComponent<IMainPageProps, {}>
    implements IMainPage {

    render(): false | JSX.Element {
        return (
            <div
                className={'main-page'}
            >
                <MainHeader/>
            </div>
        );
    }
}
