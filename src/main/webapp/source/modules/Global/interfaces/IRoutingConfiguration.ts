import { IBreadCrumbsItem } from './IBreadCrumbsItem';

export interface IRoutingConfiguration {
    breadCrumbs?: IBreadCrumbsItem[];
    parentRoute: string;
    root?: boolean;
    selfRoute?: string;
    stateName: string;
}
