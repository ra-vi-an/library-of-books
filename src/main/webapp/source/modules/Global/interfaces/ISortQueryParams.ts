export interface ISortQueryParams {
    orderBy?: string;
    orderByDirection?: string;
}
