import { IBreadCrumbsItem } from './IBreadCrumbsItem';

export interface IBreadCrumbsItems {
    npa: IBreadCrumbsItem;
    governmentRegulatoryLegalActs: IBreadCrumbsItem;
    projects: IBreadCrumbsItem;
    draftRegulatoryLegalActs: IBreadCrumbsItem;
}
