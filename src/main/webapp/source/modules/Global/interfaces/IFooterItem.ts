export interface IFooterItem {
    path: string;
    visibleName: string;
}
