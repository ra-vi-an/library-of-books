export interface IRegistrationParams {
    email?: string | null;
    phone?: string | null;
    login?: string | null;
    password?: string | null;
    firstName?: string | null;
    secondName?: string | null;
    middleName?: string | null;
}
