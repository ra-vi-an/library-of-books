export interface IMainMenuItemChild {
    name: string;
    routePath: string;
    visibleName: string;
}
