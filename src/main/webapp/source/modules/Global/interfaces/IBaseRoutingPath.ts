export interface IBaseRoutingPath {
    npa: string;
    laws: string;
    legislativeDumaResolutions: string;
    executiveAuthoritiesNormativeLegalActs: string;
    committeeOnPricesAndTariffsResolution: string;
    governmentRegulatoryLegalActs: string;
    municipalitiesNormativeLegalActs: string;
    governorResolutions: string;
    governorOrders: string;
    governmentDecrees: string;
    governmentOrders: string;
    projects: string;
    projectsSubmittedToDuma: string;
    draftLawsPublicDiscussion: string;
    executiveAuthoritiesDraftRegulatoryLegalActs: string;
    draftRegulatoryLegalActs: string;
    governorDraftResolutions: string;
    governorDraftOrders: string;
    governmentResolutionsDraft: string;
    governmentDraftOrders: string;
}
