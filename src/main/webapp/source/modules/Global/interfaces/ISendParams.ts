export interface ISendParams {
    documentFormat: string;
    email: string;
}
