export interface IMainMenuItem {
    path: string;
    visibleName: string;
}
