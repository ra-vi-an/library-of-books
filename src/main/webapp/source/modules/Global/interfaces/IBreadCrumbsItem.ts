export interface IBreadCrumbsItem {
    name: string,
    path: string,
}
