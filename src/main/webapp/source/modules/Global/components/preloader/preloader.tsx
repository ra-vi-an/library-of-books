import * as classnames from 'classnames';
import * as React from 'react';

interface IPreloaderProps {
    relativePosition?: boolean;
}

const Preloader = ({ relativePosition }: IPreloaderProps) => {
    return (
        <div
            className={classnames(
                'ws-preloader', {
                    'ws-preloader--absolute-position': !relativePosition,
                    'ws-preloader--relative-position': relativePosition,
                },
            )}
        >
            <div
                className={classnames(
                    'ws-preloader__bar',
                )}
            />
        </div>
    );
};

export default Preloader;
