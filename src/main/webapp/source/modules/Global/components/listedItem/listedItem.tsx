import { FlexBox } from '@thewhite/react-flex-layout';
import classnames from 'classnames';
import * as React from 'react';
import ReactSVG from 'react-svg';
import { CatalogAttachment } from '../../../Catalog/models/catalogAttachment';
import * as attachmentActions from '../../actions/attachmentActions';

interface IListedItemProps {
    listedItemContent: CatalogAttachment;
}

const ListedItem = ({ listedItemContent }: IListedItemProps): JSX.Element => {
    return (
        <FlexBox
            row={'start ctr'}
            className={'listed-item'}
        >
            <ReactSVG
                className={'listed-item__icon-wrapper'}
                svgClassName={'listed-item__icon'}
                src={'assets/images/svg/pdf.svg'}
            />
            <p
                className={'listed-item__content'}
            >
                <a
                    href={attachmentActions.getLinkAttachment(listedItemContent.containerId)}
                    className={classnames(
                        'listed-item__link',
                        'link-text',
                        'link-text--current',
                    )}
                >
                    {listedItemContent.name}
                </a>
            </p>
        </FlexBox>
    );
};

export { ListedItem };
