import { GlobalStore } from '@thewhite/react-global-module/lib/store';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import { WsReactBaseComponent } from '@thewhite/react-base-components';

interface IButtonProps {
    label?: string;
    globalStore?: GlobalStore;
}

interface IButton {
    props: IButtonProps;
}

@inject('globalStore')
@observer
export default class Button extends WsReactBaseComponent<IButtonProps, {}> implements IButton {
    render(): false | JSX.Element {
        return (
           <div
                className={'button'}
           >
            <button
                className={'button__button'}
            >
                { this.props.label }
            </button>
           </div>
        );
    }
}
