import * as React from 'react';
import { Helmet } from 'react-helmet';
import { globalConfig } from '../../../config/globalConfig';

const IndexHtmlConfig = () => (
    <Helmet>
        <title>Hotel</title>
        <link rel="shortcut icon" type="image/x-icon" href={'assets/images/favicon.png'}/>
        <link rel="apple-touch-icon" type="image/png" sizes="57x57" href={'assets/images/favicon.png'}/>
        <link rel="apple-touch-icon" type="image/png" sizes="114x114" href={'assets/images/favicon.png'}/>
        <link rel="apple-touch-icon" type="image/png" sizes="72x72" href={'assets/images/favicon.png'}/>
        <link rel="apple-touch-icon" type="image/png" sizes="144x144" href={'assets/images/favicon.png'}/>
        <link rel="apple-touch-icon" type="image/png" sizes="60x60" href={'assets/images/favicon.png'}/>
        <link rel="apple-touch-icon" type="image/png" sizes="120x120" href={'assets/images/favicon.png'}/>
        <link rel="apple-touch-icon" type="image/png" sizes="76x76" href={'assets/images/favicon.png'}/>
        <link rel="apple-touch-icon" type="image/png" sizes="152x152" href={'assets/images/favicon.png'}/>
    </Helmet>
);

export default IndexHtmlConfig;
