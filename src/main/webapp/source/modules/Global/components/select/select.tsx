import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { GlobalStore } from '@thewhite/react-global-module/lib/store';
import { inject, observer } from 'mobx-react';
import React from 'react';

export interface ISelectProps {
    values: any;
    selectedValue: any;
    name: string | null;
    visibleField: string;
    globalStore?: GlobalStore;
    onChange?: (value: any) => any;
}

export interface ISelect extends WsReactBaseComponentInterface {
    props: ISelectProps;
}

@inject('globalStore')
@observer
export default class Select extends WsReactBaseComponent<ISelectProps, {}> implements ISelect {
    render(): false | JSX.Element {
        return (
            <div
                className={'select'}
            >
                <div
                    className={'select__name'}
                >
                    {this.props.name}
                </div>
                <select
                    className={'select__body'}
                    onChange={this.props.onChange}
                    value={!!this.props.selectedValue ? this.props.selectedValue[this.props.visibleField] : ''}
                >
                    {
                        this.props.values.map((item: any, index: number) => {
                            return (
                                <option
                                    key={index}
                                >
                                    {
                                        !!this.props.visibleField
                                            ? item[this.props.visibleField]
                                            : item
                                    }
                                </option>
                            )
                        })
                    }
                </select>
            </div>
        );
    }
}
