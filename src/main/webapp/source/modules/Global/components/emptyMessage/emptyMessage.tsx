import classnames from 'classnames';
import * as React from 'react';

interface IEmptyMessageProps {
    additionalClass?: string;
    children: string | JSX.Element | JSX.Element[];
}

const EmptyMessage = ({ additionalClass, children }: IEmptyMessageProps): JSX.Element => {
    return (
        <section
            className={classnames(
                'empty-message', {
                    [`${additionalClass}`]: !!additionalClass,
                },
            )}
        >
            {
                typeof children === 'string'
                    ?
                        <p>{children}</p>
                    :
                        children
            }
        </section>
    );
};

export default EmptyMessage;
