import { FlexBox } from '@thewhite/react-flex-layout';
import { GlobalStore } from '@thewhite/react-global-module/lib/store';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import { WsReactBaseComponent } from '@thewhite/react-base-components';
import { IRoutingConfiguration } from '../../interfaces/IRoutingConfiguration';
import ForwardGroupLink from '../forwardGroupLink/forwardGroupLink';

interface IForwardGroupProps {
    project?: boolean;
    globalStore?: GlobalStore;
    forwarderData: IRoutingConfiguration[];
}

interface IForwardGroup {
    props: IForwardGroupProps;
}

@inject('globalStore')
@observer
export default class ForwardGroup extends WsReactBaseComponent<IForwardGroupProps, any>
    implements IForwardGroup {
    render(): false | JSX.Element | JSX.Element[] {
        return (
            <FlexBox
                <HTMLUListElement>
                rowWrap={'start stretch'}
                node={'ul'}
                className={'forward-group'}
            >
                {
                    this.props.forwarderData.map((data: IRoutingConfiguration, idx: number) => {
                        return (
                            <li
                                key={idx}
                                className={'forward-group__item'}
                            >
                                <ForwardGroupLink
                                    target={data.selfRoute ?? ''}
                                    colorful={!this.props.project}
                                >
                                    {data.stateName}
                                </ForwardGroupLink>
                            </li>
                        );
                    })
                }
            </FlexBox>
        );
    }
}
