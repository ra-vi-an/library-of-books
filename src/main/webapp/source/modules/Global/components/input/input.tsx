import { GlobalStore } from '@thewhite/react-global-module/lib/store';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import { WsReactBaseComponent } from '@thewhite/react-base-components';

interface IInputProps {
    type?: string;
    label?: string;
    placeholder?: string;
    globalStore?: GlobalStore;
    onChange?: (value: any) => any;
}

interface IInput {
    props: IInputProps;
}

@inject('globalStore')
@observer
export default class Input extends WsReactBaseComponent<IInputProps, {}> implements IInput {
    render(): false | JSX.Element {
        return (
           <div
                className={'input'}
           >
               <div
                    className={'input__title'}
               >
                   {this.props.label}
               </div>
               <input
                    className={'input__input'}
                    type={this.props.type}
                    onChange={this.props.onChange}
                    placeholder={this.props.placeholder}
               />
           </div>
        );
    }
}
