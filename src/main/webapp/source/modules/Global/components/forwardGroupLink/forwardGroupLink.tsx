import { FlexBox } from '@thewhite/react-flex-layout';
import classnames from 'classnames';
import * as React from 'react';
import ReactSVG from 'react-svg';

interface IForwardGroupLinkProps {
    colorful?: boolean;
    target: string;
    children: JSX.Element | string;
}

const ForwardGroupLink = ({ children, colorful, target }: IForwardGroupLinkProps) => {
    return (
        <FlexBox
            <HTMLAnchorElement>
            node={'a'}
            row={'start ctr'}
            href={`#${target}`}
            className={'forward-group-link'}
        >
            <ReactSVG
                className={'forward-group-link__icon-wrapper'}
                svgClassName={'forward-group-link__icon'}
                src={
                    colorful
                        ? 'assets/images/svg/folder-colorful.svg'
                        : 'assets/images/svg/folder-gray.svg'
                }
            />
            <span
                className={classnames(
                    'forward-group-link__text',
                    'text-body-3',
                )}
            >
                {children}
            </span>
            <ReactSVG
                className={'forward-group-link__icon-wrapper'}
                svgClassName={'forward-group-link__icon'}
                src={'assets/images/svg/arrow-right.svg'}
            />
        </FlexBox>
    );
};

export default ForwardGroupLink;
