import { FlexBox } from '@thewhite/react-flex-layout';
import { GlobalStore } from '@thewhite/react-global-module/lib/store';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import { WsReactBaseComponent } from '@thewhite/react-base-components';
import ReactSVG from 'react-svg';
import classnames from 'classnames';
import { MainMenuService } from '../../services/mainMenuService';
import { IMainMenuItem } from '../../interfaces/IMainMenuItem';

interface IMainMenuProps {
    globalStore?: GlobalStore;
}

interface IMainMenu {
    props: IMainMenuProps;
}

@inject('globalStore')
@observer
export default class MainMenu extends WsReactBaseComponent<IMainMenuProps, {}> implements IMainMenu {
    render(): false | JSX.Element {
        return (
            <FlexBox
                row={'ctr'}
                className={'main-menu'}
            >
                <FlexBox
                    row={'sb ctr'}
                    className={'main-menu__content'}
                >
                    <FlexBox
                        row={'ctr'}
                    >
                        <ReactSVG
                            svgClassName={'main-menu__content-svg'}
                            src={'assets/images/svg/hotel.svg'}
                            onClick={() => this.goToState('/')}
                        />
                        <div
                            className={classnames(
                                'text-subheader-2',
                                'main-menu__title',
                            )}
                            onClick={() => this.goToState('/')}
                        >
                            Гостиница
                        </div>
                        {
                            MainMenuService.mainMenu.map((mainMenu: IMainMenuItem, index: number) =>
                                <button
                                    key={index}
                                    className={'main-menu__text-left-side'}
                                    onClick={() => this.goToState(mainMenu.path)}
                                >
                                    {mainMenu.visibleName}
                                </button>,
                            )
                        }
                    </FlexBox>
                    <div
                        className={'main-menu__text-right-side'}
                    >
                        + 7 (909) 000-00-00 | Хабаровск, ул. Ленина, 1
                    </div>
                </FlexBox>
            </FlexBox>
        );
    }
}
