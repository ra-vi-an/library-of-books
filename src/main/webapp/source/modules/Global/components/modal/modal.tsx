import { WsReactBaseComponent, WsReactBaseComponentInterface } from '@thewhite/react-base-components';
import { FlexBox } from '@thewhite/react-flex-layout';
import { GlobalStore } from '@thewhite/react-global-module/lib/store';
import classnames from 'classnames';
import { inject, observer } from 'mobx-react';
import React from 'react';
import ReactSVG from 'react-svg';

export interface IModalProps {
    title: string;
    element: JSX.Element;
    globalStore?: GlobalStore;
    closeCallback: (value?: any) => any;
}

export interface IModal extends WsReactBaseComponentInterface {
    props: IModalProps;
}

@inject('globalStore')
@observer
export default class Modal extends WsReactBaseComponent<IModalProps, {}> implements IModal {
    render(): false | JSX.Element {
        return (
            <FlexBox
                row={'ctr'}
                className={'modal__wrapper'}
                onClick={this.props.closeCallback}
            >
                <div
                    className={'modal'}
                    onClick={e => e.stopPropagation()}
                >
                    <FlexBox
                        row={'sb start'}
                        className={'modal__header'}
                    >
                        <h1>{this.props.title}</h1>
                        <button
                            className={classnames(
                                'modal__close',
                                'pointer',
                            )}
                            onClick={() => this.props.closeCallback()}
                        >
                            <ReactSVG
                                className={'modal__svg'}
                                src={'../assets/images/svg/close_cross.svg'}
                            />
                        </button>
                    </FlexBox>
                    {this.props.element}
                </div>
            </FlexBox>
        )
    }
}
