import { FlexBox } from '@thewhite/react-flex-layout';
import { GlobalStore } from '@thewhite/react-global-module/lib/store';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import { WsReactBaseComponent } from '@thewhite/react-base-components';
import classnames from 'classnames';
import { FooterService } from 'source/modules/Global/services/footerService';
import { IFooterItem } from 'source/modules/Global/interfaces/IFooterItem';

interface IFooterProps {
    globalStore?: GlobalStore;
}

interface IFooter {
    props: IFooterProps;
}

@inject('globalStore')
@observer
export default class Footer extends WsReactBaseComponent<IFooterProps, {}> implements IFooter {
    render(): false | JSX.Element {
        return (
            <div
                className={'footer'}
            >
                <FlexBox
                    row={'ctr'}
                    className={'footer__content'}
                >
                    {
                        FooterService.footer.map((footer: IFooterItem, index: number) =>
                            <FlexBox
                                row={'ctr'}
                                key={index}
                                className={'footer__content-text'}
                            >
                                <div
                                    className={'footer__content-visible-name'}
                                    onClick={() => this.goToState(footer.path)}
                                >
                                  { footer.visibleName }
                                </div>
                            </FlexBox>,
                        )
                    }
                </FlexBox>
                <img
                    className={'footer__img'}
                    src={'assets/images/Hotel/main-page-bottom.png'}
                />
                <FlexBox
                    row={'ctr'}
                    className={'footer__copyright'}
                >
                    <p
                        className={classnames(
                            'text-body-2',
                        )}
                    >
                        © 2021 Copyright: Hotel.ru
                    </p>
                </FlexBox>
            </div>
        );
    }
}
