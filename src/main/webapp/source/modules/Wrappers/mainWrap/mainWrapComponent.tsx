import { WsReactBaseComponentInterface, WsReactBaseContainer } from '@thewhite/react-base-components';
import { FlexBox } from '@thewhite/react-flex-layout';
import { GlobalStore } from '@thewhite/react-global-module/lib/store';
import classnames from 'classnames';
import isEqual from 'lodash.isequal';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import { Redirect } from 'react-router-dom';
import { ToastContainer, ToastPosition } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Footer from 'source/modules/Global/components/footer/footer';
import MainMenu from 'source/modules/Global/components/mainMenu/mainMenu';
import Preloader from '../../Global/components/preloader/preloader';

interface IMainWrapProps {
    children: any;
    location: any;
    globalStore?: GlobalStore;
}

export interface IMainWrap extends WsReactBaseComponentInterface {
    props: IMainWrapProps;
    startRedirect: boolean;

    appMainContentRef: React.RefObject<HTMLDivElement>;
}

@inject('globalStore')
@observer
export class MainWrap extends WsReactBaseContainer<IMainWrapProps, {}> implements IMainWrap {
    appMainContentRef: React.RefObject<HTMLDivElement> = React.createRef();
    startRedirect: boolean = false;

    constructor(props: IMainWrapProps) {
        super(props);
        if (this.props.globalStore) {
            this.props.globalStore.handlerPressControl();
        }
    }

    componentDidMount() {
        if (this.props.globalStore) {
            this.props.globalStore.setLocation(this.props.location);
        }
    }

    componentWillReceiveProps(newProps: IMainWrapProps) {
        if (!isEqual(newProps.location, this.props.location) && !!this.props.globalStore) {
            this.props.globalStore.setLocation(newProps.location);
        }
    }

    /**
     * Получить строку со значением текущего года
     */
    getCurrentYear = (): string => {
        return this.moment()
            .year()
            .toString();
    }

    render(): false | JSX.Element {
        if (!!this.props.globalStore && !!this.props.globalStore.redirect && !this.startRedirect) {
            this.startRedirect = true;
        } else if (this.startRedirect && !!this.props.globalStore) {
            this.startRedirect = false;
            this.props.globalStore.clearRedirect();
        }

        return (
            <FlexBox
                className={classnames(
                    'main-wrap',
                )}
                row={'start stretch'}
            >
                <div
                    className={'main-wrap__content-box'}
                >
                    <MainMenu/>
                    <main
                        className={classnames(
                            'main-wrap__main-content',
                            'app-main',
                        )}
                        ref={this.appMainContentRef}
                    >
                        {
                            this.props.globalStore && this.props.globalStore.loading &&
                                <Preloader/>
                        }
                        {
                            !!this.props.globalStore && !!this.props.globalStore.redirect && this.startRedirect
                                ?
                                    <Redirect
                                        to={this.props.globalStore.redirect}
                                        push={true}
                                    />
                                :
                                    this.props.children
                        }
                    </main>
                    <Footer/>
                </div>
                <ToastContainer
                    toastClassName="global-toast"
                    position={ToastPosition.BOTTOM_RIGHT}
                />
            </FlexBox>
        );
    }
}

export default MainWrap;
