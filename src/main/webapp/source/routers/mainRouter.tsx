import { globalStore } from '@thewhite/react-global-module/lib/store';
import { Provider } from 'mobx-react';
import isEqual from 'lodash.isequal';
import * as React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import Preloader from '../modules/Global/components/preloader/preloader';
import NotFound from '../modules/Global/notFoundContainer';
import MainWrap from '../modules/Wrappers/mainWrap/mainWrapComponent';

interface IMainRouterProps {
    history?: History;
    location?: Location;
}

const stores = { globalStore };

export default class MainRouter extends React.Component<IMainRouterProps> {
    shouldComponentUpdate(nextProps: Readonly<{ history?: History; location?: Location }>): boolean {
        return !isEqual(this.props.location, nextProps.location);
    }

    render() {
        const MainPage = React.lazy(() => import('../modules/Main/pages/mainPage/mainPage'));

        return (
            <Provider
                {...stores}
            >
                <HashRouter>
                    <Switch>
                        <MainWrap
                            location={this.props.location}
                        >
                            <React.Suspense
                                fallback={
                                    <Preloader/>
                                }
                            >
                                <Switch>
                                    <Route
                                        exact
                                        path={'/'}
                                        component={MainPage}
                                    />
                                    <Route
                                        component={NotFound}
                                    />
                                </Switch>
                            </React.Suspense>
                        </MainWrap>
                        <Route
                            component={NotFound}
                        />
                    </Switch>
                </HashRouter>
            </Provider>
        );
    }
}
