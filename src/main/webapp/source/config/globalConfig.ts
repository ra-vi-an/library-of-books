import { IGlobalConfig } from './IGlobalConfig';

declare let WS_HOTEL: string;

let globalConfig: IGlobalConfig = {
    api: '',
    pageSize: 10,
    maxPageSize: 1000,
    oldestYear: 2008,
};

const variableIsDefined = (variable: any) => typeof variable !== 'undefined';
// const parseBooleanVariable = (variable: any) => variableIsDefined(variable) && !!variable && variable !== 'false';
const parseStringVariable = (variable: any, defaultValue: string) => {
    return variableIsDefined(variable) && variable ? variable : defaultValue;
}

try {
    globalConfig.api = parseStringVariable(WS_HOTEL, 'https://rivenvss-hotel.herokuapp.com/');
} catch (e) {
    globalConfig.api = 'https://rivenvss-hotel.herokuapp.com/';
}

export { globalConfig };
