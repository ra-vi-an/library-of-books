import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import * as Qs from 'qs';
import { AuthService } from '../modules/Auth/services/AuthService';

let configAxios: AxiosRequestConfig = {};

let api: AxiosInstance = axios.create(configAxios);

api.defaults.headers.Authorization = !!AuthService.getToken()
    ? `Bearer ${AuthService.getToken().access_token}`
    : 'Basic bXktY2xpZW50Om15LXNlY3JldA==';

api.interceptors.request.use((config: AxiosRequestConfig) => {
    if (!config.url) {
        return config;
    }
    try {
        config.paramsSerializer = params =>
            Qs.stringify(params, { arrayFormat: 'repeat', skipNulls: true });
    } catch (error) {
        return config;
    }
    return config;
});

export { api };
