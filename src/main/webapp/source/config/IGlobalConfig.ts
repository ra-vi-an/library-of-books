export interface IGlobalConfig {
    api: string;
    pageSize: number,
    maxPageSize: number,
    oldestYear: number,
}
